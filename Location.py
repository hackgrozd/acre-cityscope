import numpy as np


def distance(loc1, loc2): #static
    diffx = loc1.x-loc2.x
    diffy = loc1.y-loc2.y
    return np.sqrt(diffx**2 + diffy**2)

def xy_distance(x1, y1, x2, y2): # static
    diffx = x1-x2
    diffy = y1-y2
    return np.sqrt(diffx**2 + diffy**2)


class Location:

    def __init__(self, x, y, z, time):
        self.x = x
        self.y = y
        self.z = z
        self.time = time
