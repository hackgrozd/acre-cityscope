#!/usr/bin/env python

import math
import json
import numpy as np
import matplotlib as mpl
import pylab as plt
import glob

from Device import Device


def HistTimeSpentAtLoc(devices_dict, x, y, r):
    timesSpentAtLoc = []
    for device in devices_dict.values():
        timesSpentAtLoc += device.journey.timesSpentAtLoc(x, y, r)
    timesSpentAtLoc = np.array(timesSpentAtLoc) / (1000*60) # millis to mins
    plt.figure()
    plt.hist(timesSpentAtLoc, 100, (0, 60))
    plt.xlabel("t/mins")
    plt.ylabel("N")
    plt.title("Time spent at the metro entrance")
    plt.show()


#print(type(data))
#print(type(data[0]))
#print(data[0])
#print(data[0]["notifications"])

xy_str = ["x", "y"]
floor_names = ["Otaniemi>Väre>0",
                "Otaniemi>Väre>1",
                "Otaniemi>Väre>2",
                "Otaniemi>Väre>3",
                "Otaniemi>Ekonominaukio>0",
                "Otaniemi>Ekonominaukio>1",
                "Otaniemi>Ekonominaukio>2 ",
                "Otaniemi>Ekonominaukio>3",
                "Otaniemi>Ekonominaukio y-siipi>0-",
                "Otaniemi>Ekonominaukio y-siipi>1-",
                "Otaniemi>Ekonominaukio y-siipi>2 ",
                "Otaniemi>Ekonominaukio y-siipi>3"]
n_floors = len(floor_names)
floor_to_int = {floor_names[i]: i for i in range(n_floors)}

mins = {"x": [1e9] * n_floors, "y": [1e9] * n_floors}
maxs = {"x": [-1e9] * n_floors, "y": [-1e9] * n_floors}

min_x = [1e9] * n_floors
min_y = [1e9] * n_floors
max_x = [-1e9] * n_floors
max_y = [-1e9] * n_floors

minz = 1e9
maxz = -1e9

devicesMac = {}
devicesId = {}

maxFiles = 5
curFile = 0


for filepath in glob.iglob('wifidata/notify.json.*'):
    if filepath == "wifidata/notify.json.2019-11-06-04-19" \
        or filepath == "wifidata/notify.json.2019-11-09-15-58":
        continue
    curFile += 1
    if curFile > maxFiles:
        break
    print(filepath)
    with open(filepath) as f:
        data = json.load(f)
        print(len(data))
        for entry in data:
            #print(len(entry["notifications"]))
            if len(entry["notifications"]) != 1:
                print(len(entry["notifications"]))
                raise Exception("you shall not pass!")
            curData = entry["notifications"][0]
            z = floor_to_int[curData["locationMapHierarchy"].partition("krs")[0]]
            location = curData["locationCoordinate"]
            #print(curData.keys())
            curMac = curData["apMacAddress"]
            curId = curData["deviceId"]
            tempDevice = Device(curMac, curId)
            curDevice = tempDevice
            if tempDevice.hasId():
                if tempDevice.id not in devicesId.keys():
                    devicesId[tempDevice.id] = tempDevice
                else:
                    curDevice = devicesId[tempDevice.id]
            if tempDevice.hasMac():
                if tempDevice.mac not in devicesMac.keys():
                    devicesMac[tempDevice.mac] = curDevice
                else:
                    curDevice = devicesMac[tempDevice.mac]
            curDevice.addWaypoint(location["x"], location["y"], z, curData["timestamp"])
            for dir in xy_str:
                if location[dir] < mins[dir][z]:
                    mins[dir][z] = location[dir]
                if location[dir] > maxs[dir][z]:
                    maxs[dir][z] = location[dir]
            if z < minz:
                minz = z
            if z > maxz:
                maxz = z


print(len(devicesId))
print(len(devicesMac))

#for el in devicesMac.values():
#    print(el.journey.print())

print(mins)
print(maxs)
print("maxz", maxz)
print("minz", minz)

all_floors_max = {'x': max(maxs["x"]), 'y': max(maxs["y"])}
all_floors_min = {'x': min(mins["x"]), 'y': min(mins["y"])}

width = int(round(all_floors_max["x"]))-int(round(all_floors_min["x"])) +1
height = int(round(all_floors_max["y"]))-int(round(all_floors_min["y"])) +1
floor_plan = np.zeros((height, width))

test = 0
for device in devicesId.values():
    for location in device.journey.getLocations():
        test += 1
        norm_x = int(round(location.x)) - int(round(all_floors_min["x"]))
        norm_y = int(round(location.y)) - int(round(all_floors_min["y"]))
        floor_plan[norm_y, norm_x] = 1 #(location.z % 4) +1 #(location.z //3) + 1
print(test, "test")

plt.figure()
plt.imshow(floor_plan, cmap=plt.get_cmap('coolwarm'), vmin=0, vmax=1)
#plt.xticks(np.arange(-.5, width, 1))
#plt.yticks(np.arange(-.5, height, 1))
#plt.set_xticklabels(['']*L)
#plt.set_yticklabels(['']*L)
plt.title("Floor plan")
plt.xlabel("x")
plt.ylabel("y")
plt.show()

HistTimeSpentAtLoc(devicesId, 66, 42, np.sqrt((66-107)**2 + (42-60)**2))
