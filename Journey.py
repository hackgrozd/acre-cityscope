import numpy as np
import Location as L

class Journey:
    TIME_UNTIL_LOST = 1000*60*60 # 1 hour

    def __init__(self, device):
        self.device = device
        self.x_waypoints = np.array([])
        self.y_waypoints = np.array([])
        self.z_waypoints = np.array([])
        self.timestamps = np.array([])
        self.wpEntries = 0
        return


    def add(self, x, y, z, timestamp):
        idx = np.searchsorted(self.timestamps, timestamp)
        self.x_waypoints = np.insert(self.x_waypoints, idx, x)
        self.y_waypoints = np.insert(self.y_waypoints, idx, y)
        self.z_waypoints = np.insert(self.z_waypoints, idx, z)
        self.timestamps = np.insert(self.timestamps, idx, timestamp)
        #self.x_waypoints.append(x)
        #self.y_waypoints.append(y)
        #self.z_waypoints.append(int(round(z)))
        #self.timestamps.append(timestamp)
        self.wpEntries += 1
        return

    def getLocations(self):
        locations = []
        for i in range(self.wpEntries):
            locations.append(L.Location(self.x_waypoints[i], self.y_waypoints[i], self.z_waypoints[i], self.timestamps[i]))
        return locations


    def totalTimeTracked(self):
        t = 0
        for i in range(self.wpEntries-1):
            duration = self.timestamps[i+1] - self.timestamps[i]
            if duration < Journey.TIME_UNTIL_LOST:
                t += duration
        return t

    def timesSpentAtLoc(self, x, y, r):
        t = []
        tWalk = 0
        for i in range(self.wpEntries-1):
            duration = self.timestamps[i+1] - self.timestamps[i]
            if duration < Journey.TIME_UNTIL_LOST \
                    and L.xy_distance(self.x_waypoints[i], x,
                                    self.y_waypoints[i], y) < r:
                tWalk += duration
                if L.xy_distance(self.x_waypoints[i+1], x,
                                self.y_waypoints[i+1], y) >= r:
                    t.append(tWalk)
                    tWalk = 0
        return t


    def print(self):
        print("Entries of Journey:", self.wpEntries)
        for i in range(self.wpEntries):
            print(self.x_waypoints[i], self.y_waypoints[i], self.z_waypoints[i], self.timestamps[i])
