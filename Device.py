from Journey import Journey

class Device:

    def __init__(self, mac, id):
        self.mac = mac
        self.id = id
        self.journey = Journey(self)
        return


    def addWaypoint(self, x, y, z, timestamp):
        self.journey.add(x, y, z, timestamp)
        return


    def hasMac(self):
        if self.mac == "NOT APPLICABLE":
            return False
        elif self.mac == "":
            return False
        else:
            return True


    def hasId(self):
        if self.id == "NOT APPLICABLE":
            return False
        elif self.id == "":
            return False
        else:
            return True


    def print(self):
        print("Mac adress:", self.mac)
        print("Device ID:", self.id)
